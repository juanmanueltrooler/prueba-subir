function PilaStack(){
  // 1. Constructor
  // se crea el constructor con los atributos basicos que necesita la clase
  //constructor(){
    this.elementos = [];
    cadenas=[]
    tipoDatos=[]
  //}
  // 2. Insertar
  //insertar(elemento){
  this.insertar = function(elemento){
    // se agrega el elemento al arreglo
    this.elementos.push(elemento);
  }
  this.insertarPaciente=(nombre,cedula,tc){
    let paciente=[]
    
  }

  // 3. Extraer
  //extraer(){
  this.extraer = function(){
    // se toma el ultimo elemento con el tamaño - 1 recordar que los arreglos empiezan en 0 no en 1
    const ultimo = this.elementos[this.elementos.length - 1];
    // se elimina del arreglo el ultimo elemento ingresado
    this.elementos.pop();
    // devuelve el ultimo elemento
    return ultimo;
  };
  // 4. Longitud
  //longitud(){
  this.longitud = function(){
    // tamaño de la pila
    return this.elementos.length;
  }
  // 5. Ver proximo
  this.ver_proximo = function(){
    // se mira el siguiente elemento a salir de la pila pero sin eliminarlo del arreglo
    return this.elementos[this.elementos.length - 1];
  }
  // 6. Ver Elementos o toda la pila
  this.ver_elementos = function(){
    // se mira toda la pila como esta
    return this.elementos;
  }

  this.esta_vacia = function(){
    if (this.elementos.length == 0) {
      return true
    }else{
      return false
    }
  }

  this.encolarNums = function(n){
    for (var i = 1; i < n+1 ; i++) {
      this.elementos.push(i);
    }
  }

  this.saberPares = function(){
    let longitud=this.elementos.length
    let arr=[]
    for (var i = 0 ; i < longitud; i++) {
      if (this.elementos[i] % 2 == 0) {
        arr.push(0)
      }
      else{
        arr.push(1)
      }

    }
    let p=arr.length
    if (arr[0]==1 || arr[p-1] == 0){
      console.log("la secuencia par-impar es FALSE")
    }else{
      let count=0
      for(var a = 0 ; a < longitud; a++){
        if (arr[a] != arr[a+1]) {
          count=count+1
        }else{
          break
          console.log("la secuencia par-impar es FALSE")
        }

      }
      if (longitud !=0) {
        if (count==longitud){
          console.log("la secuencia par-impar es TRUE")
        }
      }
      
    }
    
  }

  this.sumar = function(){
    let total=0
    for (var i = 0 ; i < this.elementos.length; i++) {
        let num=parseInt(this.elementos[i])
        if (isNaN(num)) {
          alert("el elemento: "+this.elementos[i]+" es un string")
        }else{

          total=total+this.elementos[i]
        }
    }
    alert("total de la suma: "+total)
  }

  this.pares = function(){
    let pares=0
    let numerosPares=[]
    for (var i = this.elementos.length ; i >=0; i--) {
      if (this.elementos[i] % 2 == 0) {
        numerosPares.push(this.elementos[i])
      }
    }
    console.log("los numeros pares son: "+numerosPares)
}

  this.Impares = function(){
    let pares=0
    let numerosImpares=[]
    for (var i = this.elementos.length ; i >=0; i--) {
      if (this.elementos[i] % 2 != 0) {
        numerosImpares.push(this.elementos[i])
      }
    }

    console.log("los numeros impares son: "+numerosImpares)
  }


  this.Cadenas = function(){
    for (var i = this.elementos.length ; i>=0; i--) {
        let num=parseInt(this.elementos[i])
        if (isNaN(num)) {
          cadenas.push(this.elementos[i])
    }
    }
    console.log("los valores string son :"+cadenas)
  }


  this.tipoDatos=function(){
    for (var i = 0; i < this.elementos.length; i++) {
        tipoDatos.push(typeof this.elementos[i])
    }
    this.eliminarDuplicados()
  }

  this.eliminarDuplicados=function(){
    const result = [];
    tipoDatos.forEach((item)=>{
        if(!result.includes(item)){
            result.push(item);
        }
    })
    this.conteDatos(result)

  }

  this.conteDatos=function(){
    tipoDatos.forEach((i) => {
    var tipoDatos=(i)
    var count=0
    for (var i = 0; i < this.elementos.length; i++) {
        if(typeof this.elementos[i] == tipoDatos){
            count++;
        }
    }
    console.log("cantidad de datos de tipo:"+tipoDatos+"="+count)
    })

  }

  this.eliminarDato=function(v){
    let variable=v
    for (var i = 0; i < this.elementos.length; i++) {
        
        if(this.elementos[i] == variable){
          this.elementos.splice(i,1)
        }
    }
  }

  this.eliminarDatoPosicion=function(p){
    let variable=p
    this.elementos.splice(variable,1)
  }

  this.eliminarDatosHastaPosicion=function(p){
    let pos=p+1
    this.elementos.splice(0,pos)
  }


}

const pila = new PilaStack();

//taller 6
// pila.insertar({url:'alejo'});
// pila.insertar({url:'edwin'});
// pila.insertar({url:'laura'});
// pila.insertar({url:'sergio'});
// pila.insertar({url:'valen'});
//pila.longitud()

// pila.extraer();
// pila.extraer();
// pila.extraer();


// pila.insertar({url:'liliana'});
// pila.insertar({url:'gelmer'});
// pila.insertar({url:'camila'});
// pila.insertar({url:'valentin'});
// pila.insertar({url:'esneda'});


//pila.esta_vacia()

// console.log(pila.ver_elementos())


// const proximo = pila.ver_proximo()
// console.log(proximo)


// pila.extraer();
// pila.extraer();
// pila.extraer();
// pila.extraer();
// pila.extraer();
// pila.extraer();
// pila.extraer();

//pila.esta_vacia()

//taller7
pila.encolarNums(5)
//pila.saberPares()
//pila.sumar()
//pila.pares()
//pila.Impares()
//pila.Cadenas()
//pila.tipoDatos()
//pila.eliminarDato(2)
//pila.eliminarDatoPosicion(1)
//pila.eliminarDatosHastaPosicion(4)
pila.insertarPaciente("juan",1007220555,"n")

console.log(pila.ver_elementos())

